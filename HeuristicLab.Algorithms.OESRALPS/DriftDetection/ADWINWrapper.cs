﻿using HEAL.Attic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.DriftDetection
{
    [StorableType("719CE0E4-A88F-44A1-A2D0-92F4D8052C04")]
    class ADWINWrapper
    {
        [Storable]
        private ADWIN adwin;

        [Storable]
        private Histogram histogram;

        [Storable]
        public int Count { get; set; }
        [Storable]
        public int NumElementsProcessed { get; set; }
        [Storable]
        public int MaximumBucketsPerContainer { get; set; }

        public ADWINWrapper() { }

        public ADWINWrapper(double delta, int maximumBucketsPerContainer, int minKeepSize = 100, int minCutSize = 100)
        {
            histogram = new Histogram(maximumBucketsPerContainer);
            adwin = new SequentialADWIN(delta, minKeepSize, minCutSize);
            NumElementsProcessed = 0;
            Count = 0;
            MaximumBucketsPerContainer = maximumBucketsPerContainer;
        }

        public bool AddElement(double element)
        {
            NumElementsProcessed++;
            histogram.AddElement(element);
            Count++;
            return adwin.Execute(ref histogram);
        }


        public void Reset()
        {
            Count = 0;
        }

        public int WindowSize {
            get { return histogram.NumElements; }
        }

        public int RemoveOldestInstances()
        {
            int startElementsCount = WindowSize;
            histogram.RemoveBuckets(1);
            return startElementsCount - WindowSize;
        }
    }
}
