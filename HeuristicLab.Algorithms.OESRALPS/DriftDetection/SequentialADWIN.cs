﻿using HEAL.Attic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.DriftDetection
{
    [StorableType("714CA0E4-A88F-44D1-A2D0-92F4D8251C04")]
    public class SequentialADWIN : ADWIN
    {
        public SequentialADWIN() { }

        public SequentialADWIN(double delta, int minKeepSize = 100, int minCutSize = 100)
            : base(delta, minKeepSize, minCutSize)
        {
        }

        public override bool Execute(ref Histogram histogram)
        {
            bool tryToFindCut = true;
            bool cutFound = false;
            while (tryToFindCut)
            {
                tryToFindCut = false;           
                if (CheckHistogramForCut(histogram, histogram.GetEnumerator(), histogram.NumBuckets - 1))
                {
                    histogram.RemoveBuckets(1);
                    tryToFindCut = true;
                    cutFound = true;
                }
            }
            return cutFound;
        }

        public override void Terminate()
        {
        }
    }
}
