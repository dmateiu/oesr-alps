﻿using HEAL.Attic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.DriftDetection
{
    [StorableType("114CE0E4-A88F-44A1-A2D0-92F4D8252C04")]
    public class Bucket
    {
        public Bucket()
        {
        }

        public Bucket(double total, double variance, int numElements)
        {
            Total = total;
            Variance = variance;
            NumElements = numElements;
        }

        [Storable]
        public double Total { get; set; }
        [Storable]
        public double Variance { get; set; }
        [Storable]
        public int NumElements { get; set; }

        public double Mean {
            get {
                return Total / NumElements;
            }
        }
    }
}
