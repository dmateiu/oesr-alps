﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using HeuristicLab.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Evaluators
{
    [Item("Sliding Window Pearson R² Evaluator", "Calculates the square of the pearson correlation coefficient (also known as coefficient of determination) of a symbolic regression solution.")]
    [StorableType("6FAEC6C2-C711-452A-A60D-29AE37898A90")]
    public class SymbolicRegressionSingleObjectivePearsonRSquaredSlidingWindowEvaluator : SymbolicRegressionSingleObjectivePearsonRSquaredEvaluator
    {
        private const string TestPartitionParameterName = "TestPartition";
        private const string TrainingPartitionParameterName = "TrainingPartition";

        public ILookupParameter<IntRange> TrainingPartitionParameter {
            get { return (ILookupParameter<IntRange>)Parameters[TrainingPartitionParameterName]; }
        }
        public ILookupParameter<IntRange> TestPartitionParameter {
            get { return (ILookupParameter<IntRange>)Parameters[TestPartitionParameterName]; }
        }

        [StorableConstructor]
        protected SymbolicRegressionSingleObjectivePearsonRSquaredSlidingWindowEvaluator(StorableConstructorFlag _) : base(_) { }
        protected SymbolicRegressionSingleObjectivePearsonRSquaredSlidingWindowEvaluator(SymbolicRegressionSingleObjectivePearsonRSquaredSlidingWindowEvaluator original, Cloner cloner)
          : base(original, cloner)
        {
        }
        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionSingleObjectivePearsonRSquaredSlidingWindowEvaluator(this, cloner);
        }

        public SymbolicRegressionSingleObjectivePearsonRSquaredSlidingWindowEvaluator() : base()
        {
            Parameters.Add(new ValueLookupParameter<IntRange>(TrainingPartitionParameterName, "The current training sliding window position or range."));
            Parameters.Add(new ValueLookupParameter<IntRange>(TestPartitionParameterName, "The current test sliding window position or range."));
        }

        protected override IEnumerable<int> GenerateRowsToEvaluate(double percentageOfRows)
        {
            if (TrainingPartitionParameter.ActualValue == null
                || TestPartitionParameter.ActualValue == null)
               return base.GenerateRowsToEvaluate(percentageOfRows);

            IEnumerable<int> rows;
            int samplesStart = TrainingPartitionParameter.ActualValue.Start;
            int samplesEnd = TrainingPartitionParameter.ActualValue.End;
            int testPartitionStart = TestPartitionParameter.ActualValue.Start;
            int testPartitionEnd = TestPartitionParameter.ActualValue.End;
            if (samplesEnd < samplesStart) throw new ArgumentException("Start value is larger than end value.");

            if (percentageOfRows.IsAlmost(1.0))
                rows = Enumerable.Range(samplesStart, samplesEnd - samplesStart);
            else
            {
                int seed = RandomParameter.ActualValue.Next();
                int count = (int)((samplesEnd - samplesStart) * percentageOfRows);
                if (count == 0) count = 1;
                rows = RandomEnumerable.SampleRandomNumbers(seed, samplesStart, samplesEnd, count);
            }

            rows = rows.Where(i => i < testPartitionStart || testPartitionEnd <= i);
            if (ValidRowIndicatorParameter.ActualValue != null)
            {
                string indicatorVar = ValidRowIndicatorParameter.ActualValue.Value;
                var problemData = ProblemDataParameter.ActualValue;
                var indicatorRow = problemData.Dataset.GetReadOnlyDoubleValues(indicatorVar);
                rows = rows.Where(r => !indicatorRow[r].IsAlmost(0.0));
            }
            return rows;
        }
    }
}
