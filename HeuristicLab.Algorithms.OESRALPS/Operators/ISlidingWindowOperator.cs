﻿using HEAL.Attic;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Problems.DataAnalysis;
using System;

namespace HeuristicLab.Algorithms.OESRALPS.SlidingWindow.Operator
{
    [StorableType("3E8AA082-3B86-4609-BD38-E3FA78DAD29F")]
    public interface ISlidingWindowOperator : IOperator
    {
        IFixedValueParameter<IntValue> SlidingWindowStepWidthParameter { get; }
        IFixedValueParameter<IntValue> StartSlidingWindowParameter { get; }
        IFixedValueParameter<IntValue> SlidingWindowSizeParameter { get; }
        ILookupParameter<IntRange> FitnessCalculationPartitionParameter { get; }
        IValueLookupParameter<IntRange> TrainingPartitionParameter { get; }
        IValueLookupParameter<IntRange> ValidationPartitionParameter { get; }

        event EventHandler MoveWindow;
        IOperation OnMoveWindow();
    }
}
