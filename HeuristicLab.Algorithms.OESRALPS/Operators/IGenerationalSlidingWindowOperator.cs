﻿using HEAL.Attic;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Optimization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.SlidingWindow.Operator
{
    [StorableType("136DA41B-0AC1-49A8-B4D9-DCE73BDB7A14")]
    interface IGenerationalSlidingWindowOperator : ISlidingWindowOperator, IIterationBasedOperator
    {
        IFixedValueParameter<IntValue> GenerationsIntervalParameter { get; }
        IFixedValueParameter<IntValue> GenerationsIntervalStartParameter { get; }
    }
}
