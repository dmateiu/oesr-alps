﻿using HEAL.Attic;
using HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression;
using HeuristicLab.Algorithms.OESRALPS.Evaluators;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Problems
{
    [Item("Symbolic Regression Sliding Window MSE Problem (single-objective)", "Represents a single objective symbolic regression problem.")]
    [StorableType("7DDCF683-92FC-4A70-BF4F-FE3A0B0DE110")]
    [Creatable(CreatableAttribute.Categories.GeneticProgrammingProblems, Priority = 101)]
    class SymbolicRegressionSingleObjectiveSlidingWindowMSEProblem : SymbolicDataAnalysisSingleObjectiveProblem<IRegressionProblemData, ISymbolicRegressionSingleObjectiveEvaluator, ISymbolicDataAnalysisSolutionCreator>, IRegressionProblem
    {
        private const double PunishmentFactor = 10;
        private const int InitialMaximumTreeDepth = 12;
        private const int InitialMaximumTreeLength = 50;
        private const string EstimationLimitsParameterName = "EstimationLimits";
        private const string EstimationLimitsParameterDescription = "The limits for the estimated value that can be returned by the symbolic regression model.";

        #region parameter properties
        public IFixedValueParameter<DoubleLimit> EstimationLimitsParameter {
            get { return (IFixedValueParameter<DoubleLimit>)Parameters[EstimationLimitsParameterName]; }
        }
        #endregion
        #region properties
        public DoubleLimit EstimationLimits {
            get { return EstimationLimitsParameter.Value; }
        }
        #endregion
        [StorableConstructor]
        protected SymbolicRegressionSingleObjectiveSlidingWindowMSEProblem(StorableConstructorFlag _) : base(_) { }
        protected SymbolicRegressionSingleObjectiveSlidingWindowMSEProblem(SymbolicRegressionSingleObjectiveSlidingWindowMSEProblem original, Cloner cloner)
          : base(original, cloner)
        {
            RegisterEventHandlers();
        }
        public override IDeepCloneable Clone(Cloner cloner) { return new SymbolicRegressionSingleObjectiveSlidingWindowMSEProblem(this, cloner); }

        public SymbolicRegressionSingleObjectiveSlidingWindowMSEProblem()
          : base(new RegressionProblemData(), new SymbolicRegressionSingleObjectiveMeanSquaredErrorSlidingWindowEvaluator(), new SymbolicDataAnalysisExpressionTreeCreator())
        {
            Parameters.Add(new FixedValueParameter<DoubleLimit>(EstimationLimitsParameterName, EstimationLimitsParameterDescription));

            EstimationLimitsParameter.Hidden = true;


            ApplyLinearScalingParameter.Value.Value = true;
            Maximization.Value = false;
            MaximumSymbolicExpressionTreeDepth.Value = InitialMaximumTreeDepth;
            MaximumSymbolicExpressionTreeLength.Value = InitialMaximumTreeLength;

            RegisterEventHandlers();
            ConfigureGrammarSymbols();
            InitializeOperators();
            UpdateEstimationLimits();
        }

        [StorableHook(HookType.AfterDeserialization)]
        private void AfterDeserialization()
        {
            RegisterEventHandlers();
            // compatibility
            bool changed = false;
            if (!Operators.OfType<SymbolicRegressionSingleObjectiveTrainingParetoBestSolutionAnalyzer>().Any())
            {
                Operators.Add(new SymbolicRegressionSingleObjectiveTrainingParetoBestSolutionAnalyzer());
                changed = true;
            }
            if (!Operators.OfType<SymbolicRegressionSingleObjectiveValidationParetoBestSolutionAnalyzer>().Any())
            {
                Operators.Add(new SymbolicRegressionSingleObjectiveValidationParetoBestSolutionAnalyzer());
                changed = true;
            }
            if (!Operators.OfType<SymbolicRegressionSolutionsAnalyzer>().Any())
            {
                Operators.Add(new SymbolicRegressionSolutionsAnalyzer());
                changed = true;
            }
            if (changed)
            {
                ParameterizeOperators();
            }
        }

        private void RegisterEventHandlers()
        {
            SymbolicExpressionTreeGrammarParameter.ValueChanged += (o, e) => ConfigureGrammarSymbols();
        }

        private void ConfigureGrammarSymbols()
        {
            var grammar = SymbolicExpressionTreeGrammar as TypeCoherentExpressionGrammar;
            if (grammar != null) grammar.ConfigureAsDefaultRegressionGrammar();
        }

        private void InitializeOperators()
        {
            Operators.Add(new SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer());
            Operators.Add(new SymbolicRegressionSingleObjectiveTrainingBestSolutionAnalyzer());
            Operators.Add(new SymbolicRegressionSingleObjectiveValidationBestSolutionAnalyzer());
            Operators.Add(new SymbolicRegressionSingleObjectiveOverfittingAnalyzer());
            Operators.Add(new SymbolicRegressionSingleObjectiveTrainingParetoBestSolutionAnalyzer());
            Operators.Add(new SymbolicRegressionSingleObjectiveValidationParetoBestSolutionAnalyzer());
            Operators.Add(new SymbolicRegressionSolutionsAnalyzer());
            Operators.Add(new SymbolicExpressionTreePhenotypicSimilarityCalculator());
            Operators.Add(new SymbolicRegressionPhenotypicDiversityAnalyzer(Operators.OfType<SymbolicExpressionTreePhenotypicSimilarityCalculator>()) { DiversityResultName = "Phenotypic Diversity" });
            ParameterizeOperators();
        }

        private void UpdateEstimationLimits()
        {
            if (ProblemData.TrainingIndices.Any())
            {
                var targetValues = ProblemData.Dataset.GetDoubleValues(ProblemData.TargetVariable, ProblemData.TrainingIndices).ToList();
                var mean = targetValues.Average();
                var range = targetValues.Max() - targetValues.Min();
                EstimationLimits.Upper = mean + PunishmentFactor * range;
                EstimationLimits.Lower = mean - PunishmentFactor * range;
            }
            else
            {
                EstimationLimits.Upper = double.MaxValue;
                EstimationLimits.Lower = double.MinValue;
            }
        }

        protected override void OnProblemDataChanged()
        {
            base.OnProblemDataChanged();
            UpdateEstimationLimits();
        }

        protected override void ParameterizeOperators()
        {
            base.ParameterizeOperators();
            if (Parameters.ContainsKey(EstimationLimitsParameterName))
            {
                var operators = Parameters.OfType<IValueParameter>().Select(p => p.Value).OfType<IOperator>().Union(Operators);
                foreach (var op in operators.OfType<ISymbolicDataAnalysisBoundedOperator>())
                {
                    op.EstimationLimitsParameter.ActualName = EstimationLimitsParameter.Name;
                }
            }

            foreach (var op in Operators.OfType<ISolutionSimilarityCalculator>())
            {
                op.SolutionVariableName = SolutionCreator.SymbolicExpressionTreeParameter.ActualName;
                op.QualityVariableName = Evaluator.QualityParameter.ActualName;

                if (op is SymbolicExpressionTreePhenotypicSimilarityCalculator)
                {
                    var phenotypicSimilarityCalculator = (SymbolicExpressionTreePhenotypicSimilarityCalculator)op;
                    phenotypicSimilarityCalculator.ProblemData = ProblemData;
                    phenotypicSimilarityCalculator.Interpreter = SymbolicExpressionTreeInterpreter;
                }
            }
        }
    }
}