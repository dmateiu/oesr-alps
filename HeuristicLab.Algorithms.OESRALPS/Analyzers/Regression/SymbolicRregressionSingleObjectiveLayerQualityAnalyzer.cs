﻿using HEAL.Attic;
using HeuristicLab.Analysis;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Optimization;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRregressionSingleObjectiveLayerQualityAnalyzer", "An operator that analyzes the validation and training best symbolic regression solution for single objective symbolic regression problems considering each age layer.")]
    [StorableType("75E292AA-95B2-4FA4-8544-CF2025A65822")]
    public sealed class SymbolicRregressionSingleObjectiveLayerQualityAnalyzer
        : LayerQualityAnalyzer
    {
        [StorableConstructor]
        private SymbolicRregressionSingleObjectiveLayerQualityAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRregressionSingleObjectiveLayerQualityAnalyzer(SymbolicRregressionSingleObjectiveLayerQualityAnalyzer original, Cloner cloner)
          : base(original, cloner)
        {
        }

        public SymbolicRregressionSingleObjectiveLayerQualityAnalyzer() : base()
        {
        }

        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRregressionSingleObjectiveLayerQualityAnalyzer(this, cloner);
        }
    }
}
