﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

#region License Information
/* HeuristicLab
    * Copyright (C) Heuristic and Evolutionary Algorithms Laboratory (HEAL)
    *
    * This file is part of HeuristicLab.
    *
    * HeuristicLab is free software: you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation, either version 3 of the License, or
    * (at your option) any later version.
    *
    * HeuristicLab is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *
    * You should have received a copy of the GNU General Public License
    * along with HeuristicLab. If not, see <http://www.gnu.org/licenses/>.
    */
#endregion

using HEAL.Attic;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using HeuristicLab.Core;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Parameters;
using HeuristicLab.Common;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Random;
using HeuristicLab.Data;
using HeuristicLab.Optimization;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    /// <summary>
    /// An operator that analyzes the validation best symbolic regression solution for single objective symbolic regression problems.
    /// </summary>
    [Item("SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer", "An operator that analyzes the validation best symbolic regression solution for single objective symbolic regression problems.")]
    [StorableType("75E2AFFF-95B2-4FA4-8544-CF202A665890")]
    public class SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer 
        : SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer<ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>,
        ISymbolicDataAnalysisBoundedOperator
    {
        private const string EstimationLimitsParameterName = "EstimationLimits";
        private const string ValidationBestSolutionParameterName = "Best validation solution";
        private const string ValidationBestSolutionQualityParameterName = "Best validation solution quality";
        private const string ValidationBestSolutionGenerationParameterName = "Best validation solution generation";
        private const string UpdateAlwaysParameterName = "Always update best solution";
        private const string IterationsParameterName = "Generations";
        private const string MaximumIterationsParameterName = "Maximum Iterations";

        #region parameter properties
        public IValueLookupParameter<DoubleLimit> EstimationLimitsParameter {
            get { return (IValueLookupParameter<DoubleLimit>)Parameters[EstimationLimitsParameterName]; }
        }
        public ILookupParameter<ISymbolicRegressionSolution> ValidationBestSolutionParameter {
            get { return (ILookupParameter<ISymbolicRegressionSolution>)Parameters[ValidationBestSolutionParameterName]; }
        }
        public ILookupParameter<DoubleValue> ValidationBestSolutionQualityParameter {
            get { return (ILookupParameter<DoubleValue>)Parameters[ValidationBestSolutionQualityParameterName]; }
        }
        public ILookupParameter<IntValue> ValidationBestSolutionGenerationParameter {
            get { return (ILookupParameter<IntValue>)Parameters[ValidationBestSolutionGenerationParameterName]; }
        }
        public IFixedValueParameter<BoolValue> UpdateAlwaysParameter {
            get { return (IFixedValueParameter<BoolValue>)Parameters[UpdateAlwaysParameterName]; }
        }
        public ILookupParameter<IntValue> IterationsParameter {
            get { return (ILookupParameter<IntValue>)Parameters[IterationsParameterName]; }
        }
        public IValueLookupParameter<IntValue> MaximumIterationsParameter {
            get { return (IValueLookupParameter<IntValue>)Parameters[MaximumIterationsParameterName]; }
        }
        #endregion

        #region properties
        public ISymbolicRegressionSolution ValidationBestSolution {
            get { return ValidationBestSolutionParameter.ActualValue; }
            set { ValidationBestSolutionParameter.ActualValue = value; }
        }
        public DoubleValue ValidationBestSolutionQuality {
            get { return ValidationBestSolutionQualityParameter.ActualValue; }
            set { ValidationBestSolutionQualityParameter.ActualValue = value; }
        }
        public BoolValue UpdateAlways {
            get { return UpdateAlwaysParameter.Value; }
        }
        #endregion

        [StorableConstructor]
        private SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer(SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer()
            : base()
        {
            Parameters.Add(new LookupParameter<ISymbolicRegressionSolution>(ValidationBestSolutionParameterName, "The validation best symbolic data analyis solution."));
            Parameters.Add(new LookupParameter<DoubleValue>(ValidationBestSolutionQualityParameterName, "The quality of the validation best symbolic data analysis solution."));
            Parameters.Add(new LookupParameter<IntValue>(ValidationBestSolutionGenerationParameterName, "The generation in which the best validation solution was found."));
            Parameters.Add(new FixedValueParameter<BoolValue>(UpdateAlwaysParameterName, "Determines if the best validation solution should always be updated regardless of its quality.", new BoolValue(true)));
            Parameters.Add(new LookupParameter<IntValue>(IterationsParameterName, "The number of performed iterations."));
            Parameters.Add(new ValueLookupParameter<IntValue>(MaximumIterationsParameterName, "The maximum number of performed iterations.") { Hidden = true });
            UpdateAlwaysParameter.Hidden = true;
            Parameters.Add(new ValueLookupParameter<DoubleLimit>(EstimationLimitsParameterName, "The lower and upper limit for the estimated values produced by the symbolic regression model."));
        }

        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionSingleObjectiveValidationBestSolutionSlidingWindowAnalyzer(this, cloner);
        }

        protected ISymbolicRegressionSolution CreateSolution(ISymbolicExpressionTree bestTree, double bestQuality)
        {
            var model = new SymbolicRegressionModel(ProblemDataParameter.ActualValue.TargetVariable, (ISymbolicExpressionTree)bestTree.Clone(), SymbolicDataAnalysisTreeInterpreterParameter.ActualValue, EstimationLimitsParameter.ActualValue.Lower, EstimationLimitsParameter.ActualValue.Upper);
            if (ApplyLinearScalingParameter.ActualValue.Value) model.Scale(ProblemDataParameter.ActualValue);
            return new SymbolicRegressionSolution(model, (IRegressionProblemData)ProblemDataParameter.ActualValue.Clone());
        }

        protected override IEnumerable<int> GenerateRowsToEvaluate()
        {
            if (ValidationPartitionParameter.ActualValue == null
                || TestPartitionParameter.ActualValue == null)
                return base.GenerateRowsToEvaluate();

            int seed = RandomParameter.ActualValue.Next();
            int samplesStart = ValidationPartitionParameter.ActualValue.Start;
            int samplesEnd = ValidationPartitionParameter.ActualValue.End;
            int testPartitionStart = TestPartitionParameter.ActualValue.Start;
            int testPartitionEnd = TestPartitionParameter.ActualValue.End;

            if (samplesEnd < samplesStart) throw new ArgumentException("Start value is larger than end value.");
            int count = (int)((samplesEnd - samplesStart) * RelativeNumberOfEvaluatedSamplesParameter.ActualValue.Value);
            if (count == 0) count = 1;
            return RandomEnumerable.SampleRandomNumbers(seed, samplesStart, samplesEnd, count)
                .Where(i => i < testPartitionStart && i < ProblemDataParameter.ActualValue.Dataset.Rows);
        }

        public override IOperation Apply()
        {
            IEnumerable<int> rows = GenerateRowsToEvaluate();
            if (!rows.Any()) return base.Apply();

            #region find best tree
            var evaluator = EvaluatorParameter.ActualValue;
            var problemData = ProblemDataParameter.ActualValue;
            double bestValidationQuality = Maximization.Value ? double.NegativeInfinity : double.PositiveInfinity;
            ISymbolicExpressionTree bestTree = null;
            ISymbolicExpressionTree[] tree = SymbolicExpressionTree.ToArray();

            // sort is ascending and we take the first n% => order so that best solutions are smallest
            // sort order is determined by maximization parameter
            double[] trainingQuality;
            if (Maximization.Value)
            {
                // largest values must be sorted first
                trainingQuality = Quality.Select(x => -x.Value).ToArray();
            }
            else
            {
                // smallest values must be sorted first
                trainingQuality = Quality.Select(x => x.Value).ToArray();
            }

            // sort trees by training qualities
            Array.Sort(trainingQuality, tree);

            // number of best training solutions to validate (at least 1)
            int topN = (int)Math.Max(tree.Length * PercentageOfBestSolutionsParameter.ActualValue.Value, 1);

            IExecutionContext childContext = (IExecutionContext)ExecutionContext.CreateChildOperation(evaluator);
            // evaluate best n training trees on validiation set
            var quality = tree
              .Take(topN)
              .Select(t => evaluator.Evaluate(childContext, t, problemData, rows))
              .ToArray();

            for (int i = 0; i < quality.Length; i++)
            {
                if (IsBetter(quality[i], bestValidationQuality, Maximization.Value))
                {
                    bestValidationQuality = quality[i];
                    bestTree = tree[i];
                }
            }
            #endregion

            var results = ResultCollection;
            if (bestTree != null && (UpdateAlways.Value || ValidationBestSolutionQuality == null ||
              IsBetter(bestValidationQuality, ValidationBestSolutionQuality.Value, Maximization.Value)))
            {
                ValidationBestSolution = CreateSolution(bestTree, bestValidationQuality);
                ValidationBestSolutionQuality = new DoubleValue(bestValidationQuality);
                if (IterationsParameter.ActualValue != null)
                    ValidationBestSolutionGenerationParameter.ActualValue = new IntValue(IterationsParameter.ActualValue.Value);

                if (!results.ContainsKey(ValidationBestSolutionParameter.Name))
                {
                    results.Add(new Result(ValidationBestSolutionParameter.Name, ValidationBestSolutionParameter.Description, ValidationBestSolution));
                    results.Add(new Result(ValidationBestSolutionQualityParameter.Name, ValidationBestSolutionQualityParameter.Description, ValidationBestSolutionQuality));
                    if (ValidationBestSolutionGenerationParameter.ActualValue != null)
                        results.Add(new Result(ValidationBestSolutionGenerationParameter.Name, ValidationBestSolutionGenerationParameter.Description, ValidationBestSolutionGenerationParameter.ActualValue));
                }
                else
                {
                    results[ValidationBestSolutionParameter.Name].Value = ValidationBestSolution;
                    results[ValidationBestSolutionQualityParameter.Name].Value = ValidationBestSolutionQuality;
                    if (ValidationBestSolutionGenerationParameter.ActualValue != null)
                        results[ValidationBestSolutionGenerationParameter.Name].Value = ValidationBestSolutionGenerationParameter.ActualValue;
                }
            }
            return base.Apply();
        }
        private bool IsBetter(double lhs, double rhs, bool maximization)
        {
            if (maximization) return lhs > rhs;
            else return lhs < rhs;
        }
    }
}

