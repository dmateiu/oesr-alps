﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer", "An operator that analyzes the correlation of training and validation fitness of symbolic regression models and moves a sliding window if the thresholds are exceeded.")]
    [StorableType("7BE112AA-94B2-4BA4-8544-AF2025A65122")]
    public sealed class SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer 
        : OverfittingSlidingWindowAnalyzer<ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>
    {
        [StorableConstructor]
        private SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer(SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer()
          : base()
        {
        }
        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionOverfittingSelectionSlidingWindowAnalyzer(this, cloner);
        }
    }
}
