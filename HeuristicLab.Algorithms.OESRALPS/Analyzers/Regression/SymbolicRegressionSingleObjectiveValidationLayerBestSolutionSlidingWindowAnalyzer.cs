﻿using HEAL.Attic;
using HeuristicLab.Algorithms.OESRALPS.SlidingWindow.Operator;
using HeuristicLab.Analysis;
using HeuristicLab.Collections;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Operators;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using HeuristicLab.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer", "An operator that analyzes the validation best symbolic regression solution for single objective symbolic regression problems considering each age layer.")]
    [StorableType("75E2A19A-95B2-4FA4-8544-CF2325B65820")]
    public class SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer
        : SymbolicDataAnalysisSingleObjectiveValidationLayerBestSolutionAnalyzer<ISymbolicRegressionSolution, ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>
    {

        [StorableConstructor]
        protected SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer(StorableConstructorFlag _)
            : base(_) { }
        protected SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer(SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer()
          : base()
        {
        }

        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionSingleObjectiveValidationLayerBestSolutionSlidingWindowAnalyzer(this, cloner);
        }

        protected override ISymbolicRegressionSolution CreateSolution(ISymbolicExpressionTree bestTree, double bestQuality)
        {
            var model = new SymbolicRegressionModel(ProblemDataParameter.ActualValue.TargetVariable, (ISymbolicExpressionTree)bestTree.Clone(), SymbolicDataAnalysisTreeInterpreterParameter.ActualValue, EstimationLimitsParameter.ActualValue.Lower, EstimationLimitsParameter.ActualValue.Upper);
            if (ApplyLinearScalingParameter.ActualValue.Value) model.Scale(ProblemDataParameter.ActualValue);
            return new SymbolicRegressionSolution(model, (IRegressionProblemData)ProblemDataParameter.ActualValue.Clone());
        }
    }
}