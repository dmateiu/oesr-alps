﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionGenerationalSlidingWindowAnalyzer", "Symbolic Regression Analyzer which moves a sliding window every n-th generation over the training partition.")]
    [StorableType("3E8EA052-3116-4610-BA18-E3FE78DAD22F")]
    public sealed class SymbolicRegressionGenerationalSlidingWindowAnalyzer : GenerationalSlidingWindowAnalyzer<ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>
    {
        [StorableConstructor]
        private SymbolicRegressionGenerationalSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRegressionGenerationalSlidingWindowAnalyzer(SymbolicRegressionGenerationalSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionGenerationalSlidingWindowAnalyzer()
          : base()
        {
        }
        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionGenerationalSlidingWindowAnalyzer(this, cloner);
        }
    }
}
