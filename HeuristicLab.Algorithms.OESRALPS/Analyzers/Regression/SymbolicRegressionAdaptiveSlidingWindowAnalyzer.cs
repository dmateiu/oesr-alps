﻿using HEAL.Attic;
using HeuristicLab.Algorithms.OESRALPS.DriftDetection;
using HeuristicLab.Analysis;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Operators;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionGenerationalAdaptiveSlidingWindowAnalyzer", "Symbolic Regression Analyzer which moves a sliding window every n-th generation over the training partition and adapts the window size, when a change is detected.")]
    [StorableType("936D341B-0AC1-49A8-B4D9-DCE13BDB7114")]
    public sealed class SymbolicRegressionAdaptiveSlidingWindowAnalyzer : SlidingWindowAnalyzer<ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>
    {
        private const string LayerResultsParameterName = "LayerResults";

        private const string DriftDetectedParameterName = "Drift Detected";
        private const string AdwinWindowSizeParameterName = "ADWIN Window Size";

        private const string SlidingWindowSizeChart = "Adaptive Sliding Window Size";

        private const string DeltaParameterName = "Delta";

        [Storable]
        private ADWINWrapper adwin;

        #region parameter properties
        public IScopeTreeLookupParameter<ResultCollection> LayerResultsParameterParameter {
            get { return (IScopeTreeLookupParameter<ResultCollection>)Parameters[LayerResultsParameterName]; }
        }
        public IFixedValueParameter<DoubleValue> DeltaParameter {
            get { return (IFixedValueParameter<DoubleValue>)Parameters[DeltaParameterName]; }
        }
        #endregion

        [StorableConstructor]
        private SymbolicRegressionAdaptiveSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRegressionAdaptiveSlidingWindowAnalyzer(SymbolicRegressionAdaptiveSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionAdaptiveSlidingWindowAnalyzer()
          : base()
        {
            Parameters.Add(new ScopeTreeLookupParameter<ResultCollection>(LayerResultsParameterName, "Results of all Layers.") { Depth = 1 });
            Parameters.Add(new FixedValueParameter<DoubleValue>(DeltaParameterName, "The confidence value for hypothesis test.", new DoubleValue(0.1)));

            SlidingWindowSize.Value = 100;
            SlidingWindowStepWidth.Value = (int)(SlidingWindowSize.Value * 0.1);

            SlidingWindowSizeParameter.Hidden = true;
            SlidingWindowStepWidthParameter.Hidden = true;

            adwin = new ADWINWrapper(DeltaParameter.Value.Value, 200, 20, 20);
        }
        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionAdaptiveSlidingWindowAnalyzer(this, cloner);
        }

        public override IOperation Apply()
        {
            #region DataTable Parameters
            var results = ResultCollection;
            if (!results.ContainsKey(SlidingWindowSizeChart))
                results.Add(new Result(SlidingWindowSizeChart, new DataTable(SlidingWindowSizeChart)));

            var slidingWindowSizeChart = (DataTable)results[SlidingWindowSizeChart].Value;

            if (!slidingWindowSizeChart.Rows.ContainsKey(AdwinWindowSizeParameterName))
                slidingWindowSizeChart.Rows.Add(new DataRow(AdwinWindowSizeParameterName));
            if (!slidingWindowSizeChart.Rows.ContainsKey(DriftDetectedParameterName))
                slidingWindowSizeChart.Rows.Add(new DataRow(DriftDetectedParameterName) { VisualProperties = { SecondYAxis = true } });
            #endregion

            if (TrainingPartitionParameter.ActualValue == null 
                || ValidationPartitionParameter == null) { 
                adwin = new ADWINWrapper(DeltaParameter.Value.Value, 200, 20, 20);
                SlidingWindowSize.Value = 100;
                SlidingWindowStepWidth.Value = (int)(SlidingWindowSize.Value * 0.1);
                TerminateSlidingWindowParameter.ActualValue.Value = false;
                InitializeSlidingWindow(StartSlidingWindow.Value, SlidingWindowSize.Value);
                return base.Apply();
            }
            if (!IsMinimumIterationIntervalPassed())
                return base.Apply();

            #region set up evaluation
            var growingTerm = 1 + (int)(SlidingWindowSize.Value * 0.10); //1 + (int)(SlidingWindowStepWidth.Value * 0.5);
            if (ValidationPartitionParameter.ActualValue.End >= ProblemData.TestPartition.Start)
            {
                TerminateSlidingWindowParameter.ActualValue.Value = true;
                return base.Apply();
            }
            else if (ValidationPartitionParameter.ActualValue.End > ProblemData.TestPartition.Start - growingTerm)
            {
                growingTerm = ProblemData.TestPartition.Start - ValidationPartitionParameter.ActualValue.End;
            }
            
            IntRange rangeToEvaluate;
            if (adwin.NumElementsProcessed == 0)
                rangeToEvaluate = new IntRange(adwin.Count,
                    ValidationPartitionParameter.ActualValue.End + growingTerm + 1);
            else
                rangeToEvaluate =
                    new IntRange(adwin.Count,
                    ValidationPartitionParameter.ActualValue.End + growingTerm);

            var rows = Enumerable.Range(rangeToEvaluate.Start, rangeToEvaluate.Size);
            #endregion
            
            var changeDetected = false;
            var targetValues = ProblemDataParameter.ActualValue.Dataset.GetDoubleValues(ProblemDataParameter.ActualValue.TargetVariable, rows);

            IList<double> changeBuffer = new List<double>();
            IList<double> windowSizeBuffer = new List<double>();

            var targetValuesEnumerator = targetValues.GetEnumerator();
            while (!changeDetected && targetValuesEnumerator.MoveNext())
            {
                changeDetected |= adwin.AddElement(targetValuesEnumerator.Current);
                //changeBuffer.Add(changeDetected ? 1 : 0);
                //windowSizeBuffer.Add(adwin.WindowSize);
            }

            InitializeSlidingWindow(
                adwin.Count - adwin.WindowSize,
                adwin.WindowSize);

            SlidingWindowSize.Value = adwin.WindowSize;
            SlidingWindowStepWidth.Value = (int)(SlidingWindowSize.Value * 0.1);

            //slidingWindowSizeChart.Rows[DriftDetectedParameterName].Values.AddRange(changeBuffer);
            //slidingWindowSizeChart.Rows[AdwinWindowSizeParameterName].Values.AddRange(windowSizeBuffer);
            results[SlidingWindowSizeChart].Value = slidingWindowSizeChart;

            return base.Apply();
        }
    }
}
