﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer", "An operator that analyzes the training best symbolic regression solution for single objective symbolic regression problems.")]
    [StorableType("85786F8E-A81D-4909-9A66-620669A0C7FB")]
    public sealed class SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer 
        : SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer<ISymbolicRegressionSolution, ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>,
        ISymbolicDataAnalysisInterpreterOperator, ISymbolicDataAnalysisBoundedOperator
    {
        private const string ProblemDataParameterName = "ProblemData";
        private const string SymbolicDataAnalysisTreeInterpreterParameterName = "SymbolicExpressionTreeInterpreter";
        private const string EstimationLimitsParameterName = "EstimationLimits";

        #region parameter properties
        public IValueLookupParameter<DoubleLimit> EstimationLimitsParameter {
            get { return (IValueLookupParameter<DoubleLimit>)Parameters[EstimationLimitsParameterName]; }
        }
        #endregion

        [StorableConstructor]
        private SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer(SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer()
          : base()
        {
            Parameters.Add(new ValueLookupParameter<DoubleLimit>(EstimationLimitsParameterName, "The lower and upper limit for the estimated values produced by the symbolic regression model."));
        }
        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer(this, cloner);
        }

        protected override ISymbolicRegressionSolution CreateSolution(ISymbolicExpressionTree bestTree, double bestQuality)
        {
            var model = new SymbolicRegressionModel(ProblemDataParameter.ActualValue.TargetVariable, (ISymbolicExpressionTree)bestTree.Clone(), SymbolicDataAnalysisTreeInterpreterParameter.ActualValue, EstimationLimitsParameter.ActualValue.Lower, EstimationLimitsParameter.ActualValue.Upper);
            if (ApplyLinearScalingParameter.ActualValue.Value) model.Scale(ProblemDataParameter.ActualValue);
            return new SymbolicRegressionSolution(model, (IRegressionProblemData)ProblemDataParameter.ActualValue.Clone());
        }
    }
}
