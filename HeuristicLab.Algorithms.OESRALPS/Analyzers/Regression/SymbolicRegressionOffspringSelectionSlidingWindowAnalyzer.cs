﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer", "An operator that analyzes the offspring selection pressure and moves a sliding window if the threshold is exceeded.")]
    [StorableType("75E2AAAA-95B2-4FA4-8544-CC2021B65820")]
    public class SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer 
        : OffspringSelectionSlidingWindowAnalyzer<ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>
    {
        [StorableConstructor]
        private SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        private SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer(SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer()
          : base()
        {
        }
        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionOffspringSelectionSlidingWindowAnalyzer(this, cloner);
        }
    }
}
