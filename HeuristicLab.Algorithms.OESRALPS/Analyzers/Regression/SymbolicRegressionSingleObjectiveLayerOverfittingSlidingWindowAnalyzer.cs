﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic.Regression;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers.Regression
{
    [Item("SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer", "Calculates and tracks correlation of training and validation fitness of symbolic regression models.")]
    [StorableType("75E2AAA3-95B2-4FA4-8554-CF2025B67729")]
    public sealed class SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer 
        : SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer<ISymbolicRegressionSingleObjectiveEvaluator, IRegressionProblemData>
    {
        [StorableConstructor]
        private SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer(StorableConstructorFlag _)
               : base(_) { }
        private SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer(SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer original, Cloner cloner) : base(original, cloner) { }
        public SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer()
          : base()
        {
        }

        public override IDeepCloneable Clone(Cloner cloner)
        {
            return new SymbolicRegressionSingleObjectiveOverfittingSlidingWindowAnalyzer(this, cloner);
        }
    }
}
