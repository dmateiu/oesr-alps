﻿using HEAL.Attic;
using HeuristicLab.Algorithms.OESRALPS.SlidingWindow.Operator;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [StorableType("136DA11B-0AC1-49A8-B4D9-DCE73BDB7114")]
    public interface ISlidingWindowAnalyzer : 
        ISymbolicDataAnalysisSingleObjectiveAnalyzer, ISlidingWindowOperator
    {
    }
}
