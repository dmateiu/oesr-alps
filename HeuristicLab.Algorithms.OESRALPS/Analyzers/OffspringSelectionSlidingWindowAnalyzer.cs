﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [Item("OffspringSelectionSlidingWindowAnalyzer", "An operator that analyzes the offspring selection pressure and moves a sliding window if the threshold is exceeded.")]
    [StorableType("75E112AA-95B2-4BA4-8544-CF2025A65822")]
    public abstract class OffspringSelectionSlidingWindowAnalyzer<T, U> : SlidingWindowAnalyzer<T, U>
        where T : class, ISymbolicDataAnalysisSingleObjectiveEvaluator<U>
        where U : class, IDataAnalysisProblemData
    { 
        private const string SelectionPressureParameterName = "SelectionPressure";
        private const string SelectionPressureThresholdParameterName = "SelectionPressureThreshold";

        #region parameter properties
        public IScopeTreeLookupParameter<DoubleValue> SelectionPressureParameter {
            get { return (IScopeTreeLookupParameter<DoubleValue>)Parameters[SelectionPressureParameterName]; }
        }
        public IFixedValueParameter<DoubleValue> SelectionPressureThresholdParameter {
            get { return (IFixedValueParameter<DoubleValue>)Parameters[SelectionPressureThresholdParameterName]; }
        }
        #endregion

        #region properties
        public DoubleValue SelectionPressureThreshold {
            get { return SelectionPressureThresholdParameter.Value; }
        }
        public ItemArray<DoubleValue> SelectionPressure {
            get { return SelectionPressureParameter.ActualValue; }
        }
        #endregion

        [StorableConstructor]
        protected OffspringSelectionSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        protected OffspringSelectionSlidingWindowAnalyzer(OffspringSelectionSlidingWindowAnalyzer<T, U> original, Cloner cloner) : base(original, cloner) { }
        public OffspringSelectionSlidingWindowAnalyzer()
        {
            Parameters.Add(new ScopeTreeLookupParameter<DoubleValue>(SelectionPressureParameterName, "Offspring selection pressure."));
            Parameters.Add(new FixedValueParameter<DoubleValue>(SelectionPressureThresholdParameterName, "Offspring selection pressure threshold.", new DoubleValue(20)));
        }

        public override IOperation Apply()
        {
            if (TrainingPartitionParameter.ActualValue == null
                || IsSelectionPressureThresholdExceeded())
                return OnMoveWindow();
             
            return base.Apply();
        }

        private bool IsSelectionPressureThresholdExceeded()
        {
            // Selection pressure on each layer higher
            // TODO consider maximum Selection Pressure
            var AgeLimits = 
                AgingScheme
                .Linear
                .CalculateAgeLimits((int)SelectionPressureThreshold.Value, SelectionPressure.Count());

            // TODO think of a suitable strategy, 
            // leave out first layer or check only last iterations before moving to next layer
            for (int i = 0; i < SelectionPressure.Count(); i++)
            {
                if (SelectionPressure[i].Value > AgeLimits[i])
                    return true;                
            }

            return false;    
        }
    }
}
