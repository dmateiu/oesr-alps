﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using System;
using System.Collections.Generic;
using HeuristicLab.Analysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeuristicLab.Optimization;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [Item("SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer", "Calculates and tracks correlation of training and validation fitness of symbolic regression models.")]
    [StorableType("AE1F0B73-BEB1-47AF-8ECF-DBCFA32AA5B9")]
    public abstract class SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer<T, U>
        : SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer<T, U>
        where T : class, ISymbolicDataAnalysisSingleObjectiveEvaluator<U>
        where U : class, IDataAnalysisProblemData
    {
        private const string TrainingValidationCorrelationParameterName = "Training and validation fitness correlation";
        private const string TrainingValidationCorrelationTableParameterName = "Training and validation fitness correlation table";
        private const string LowerCorrelationThresholdParameterName = "LowerCorrelationThreshold";
        private const string UpperCorrelationThresholdParameterName = "UpperCorrelationThreshold";
        private const string OverfittingParameterName = "IsOverfitting";

        #region parameter properties
        public ILookupParameter<DoubleValue> TrainingValidationQualityCorrelationParameter {
            get { return (ILookupParameter<DoubleValue>)Parameters[TrainingValidationCorrelationParameterName]; }
        }
        public ILookupParameter<DataTable> TrainingValidationQualityCorrelationTableParameter {
            get { return (ILookupParameter<DataTable>)Parameters[TrainingValidationCorrelationTableParameterName]; }
        }
        public IValueLookupParameter<DoubleValue> LowerCorrelationThresholdParameter {
            get { return (IValueLookupParameter<DoubleValue>)Parameters[LowerCorrelationThresholdParameterName]; }
        }
        public IValueLookupParameter<DoubleValue> UpperCorrelationThresholdParameter {
            get { return (IValueLookupParameter<DoubleValue>)Parameters[UpperCorrelationThresholdParameterName]; }
        }
        public ILookupParameter<BoolValue> OverfittingParameter {
            get { return (ILookupParameter<BoolValue>)Parameters[OverfittingParameterName]; }
        }
        #endregion

        [StorableConstructor]
        protected SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer(StorableConstructorFlag _) : base(_) { }
        protected SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer(SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer<T, U> original, Cloner cloner) : base(original, cloner) { }
        public SymbolicDataAnalysisSingleObjectiveOverfittingAnalyzer()
          : base()
        {
            Parameters.Add(new LookupParameter<DoubleValue>(TrainingValidationCorrelationParameterName, "Correlation of training and validation fitnesses"));
            Parameters.Add(new LookupParameter<DataTable>(TrainingValidationCorrelationTableParameterName, "Data table of training and validation fitness correlation values over the whole run."));
            Parameters.Add(new ValueLookupParameter<DoubleValue>(LowerCorrelationThresholdParameterName, "Lower threshold for correlation value that marks the boundary from non-overfitting to overfitting.", new DoubleValue(0.65)));
            Parameters.Add(new ValueLookupParameter<DoubleValue>(UpperCorrelationThresholdParameterName, "Upper threshold for correlation value that marks the boundary from overfitting to non-overfitting.", new DoubleValue(0.75)));
            Parameters.Add(new LookupParameter<BoolValue>(OverfittingParameterName, "Boolean indicator for overfitting."));
        }

        public override IOperation Apply()
        {
            IEnumerable<int> rows = GenerateRowsToEvaluate();
            if (!rows.Any()) return base.Apply();

            double[] trainingQuality = QualityParameter.ActualValue.Select(x => x.Value).ToArray();
            var problemData = ProblemDataParameter.ActualValue;
            var evaluator = EvaluatorParameter.ActualValue;

            // evaluate on validation partition
            IExecutionContext childContext = (IExecutionContext)ExecutionContext.CreateChildOperation(evaluator);
            double[] validationQuality = SymbolicExpressionTree
              .Select(t => evaluator.Evaluate(childContext, t, problemData, rows))
              .ToArray();

            double r = 0.0;
            try
            {
                r = alglib.spearmancorr2(trainingQuality, validationQuality);
            }
            catch (alglib.alglibexception)
            {
                r = 0.0;
            }

            var results = ResultCollection;
            #region Add Parameters
            if (!results.ContainsKey(TrainingValidationQualityCorrelationTableParameter.Name))
                ResultCollectionParameter.ActualValue.Add(new Result(TrainingValidationQualityCorrelationTableParameter.Name, TrainingValidationQualityCorrelationTableParameter.Description, typeof(DataTable)));
            if (!results.ContainsKey(OverfittingParameter.Name))
                results.Add(new Result(OverfittingParameter.Name, OverfittingParameter.Description, typeof(BoolValue)));
            #endregion

            TrainingValidationQualityCorrelationParameter.ActualValue = new DoubleValue(r);

            if (TrainingValidationQualityCorrelationTableParameter.ActualValue == null)
            {
                var dataTable = new DataTable(TrainingValidationQualityCorrelationTableParameter.Name, TrainingValidationQualityCorrelationTableParameter.Description);
                dataTable.Rows.Add(new DataRow(TrainingValidationQualityCorrelationParameter.Name, TrainingValidationQualityCorrelationParameter.Description));
                dataTable.Rows[TrainingValidationQualityCorrelationParameter.Name].VisualProperties.StartIndexZero = true;
                TrainingValidationQualityCorrelationTableParameter.ActualValue = dataTable;
            }

            TrainingValidationQualityCorrelationTableParameter.ActualValue.Rows[TrainingValidationQualityCorrelationParameter.Name].Values.Add(r);

            if (OverfittingParameter.ActualValue != null && OverfittingParameter.ActualValue.Value)
            {
                // overfitting == true
                // => r must reach the upper threshold to switch back to non-overfitting state
                OverfittingParameter.ActualValue = new BoolValue(r < UpperCorrelationThresholdParameter.ActualValue.Value);
            }
            else
            {
                // overfitting == false 
                // => r must drop below lower threshold to switch to overfitting state
                OverfittingParameter.ActualValue = new BoolValue(r < LowerCorrelationThresholdParameter.ActualValue.Value);
            }

            results[TrainingValidationQualityCorrelationTableParameter.Name].Value = TrainingValidationQualityCorrelationTableParameter.ActualValue;
            results[OverfittingParameter.Name].Value = OverfittingParameter.ActualValue;

            return base.Apply();
        }
    }
}
