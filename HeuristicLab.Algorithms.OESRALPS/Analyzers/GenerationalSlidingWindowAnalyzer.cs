﻿using HEAL.Attic;
using HeuristicLab.Algorithms.OESRALPS.SlidingWindow.Operator;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [Item("GenerationalSlidingWindowAnalyzer", "Analyzer which moves a sliding window every n-th generation over the training partition.")]
    [StorableType("3E8EA052-3A86-4610-BA18-E3FE78DAD22F")]
    public abstract class GenerationalSlidingWindowAnalyzer<T, U> : SlidingWindowAnalyzer<T, U>, IGenerationalSlidingWindowOperator
        where T : class, ISymbolicDataAnalysisSingleObjectiveEvaluator<U>
        where U : class, IDataAnalysisProblemData
    {
        private const string GenerationsIntervalParameterName = "GenerationInterval";
        private const string GenerationsIntervalStartParameterName = "GenerationIntervalStart";

        #region parameter properties
        public IFixedValueParameter<IntValue> GenerationsIntervalParameter {
            get { return (IFixedValueParameter<IntValue>)Parameters[GenerationsIntervalParameterName]; }
        }
        public IFixedValueParameter<IntValue> GenerationsIntervalStartParameter {
            get { return (IFixedValueParameter<IntValue>)Parameters[GenerationsIntervalStartParameterName]; }
        }
        #endregion

        #region properties
        public IntValue GenerationsInterval { get { return GenerationsIntervalParameter.Value; } }
        public IntValue GenerationsIntervalStart { get { return GenerationsIntervalStartParameter.Value; } }
        #endregion

        [StorableConstructor]
        protected GenerationalSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        protected GenerationalSlidingWindowAnalyzer(GenerationalSlidingWindowAnalyzer<T, U> original, Cloner cloner) : base(original, cloner) { }
        public GenerationalSlidingWindowAnalyzer()
          : base()
        {
            Parameters.Add(new FixedValueParameter<IntValue>(GenerationsIntervalParameterName, "The number of generations to pass for a window move.", new IntValue(1)));
            Parameters.Add(new FixedValueParameter<IntValue>(GenerationsIntervalStartParameterName, "The number of generations to pass before the first window move.", new IntValue(0)));

            MinimumIterationsUntilNextMoveParameter.Value = GenerationsIntervalParameter.Value;
            MinimumIterationsUntilNextMoveParameter.Hidden = true;
        }
        
        public override IOperation Apply()
        {
            if (TrainingPartitionParameter.ActualValue == null
                || IsIterationIntervalPassed())
                return OnMoveWindow();

            return base.Apply();
        }

        protected IOperation GetBaseOperation()
        {
            return base.Apply();
        }

        protected bool IsIterationIntervalPassed() {
            return (IterationsParameter.ActualValue.Value - GenerationsIntervalStart.Value) % GenerationsInterval.Value == 0;
        }
    }
}
