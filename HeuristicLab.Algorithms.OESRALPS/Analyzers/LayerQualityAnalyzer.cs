﻿using HEAL.Attic;
using HeuristicLab.Analysis;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [Item("LayerQualityAnalyzer", "An operator that analyzes the validation and training best solution for single objective problems on a layer.")]
    [StorableType("31E292AA-95B2-4FA4-8544-CF2025B65822")]
    public abstract class LayerQualityAnalyzer : SymbolicDataAnalysisSingleObjectiveAnalyzer
    {
        protected const string TrainingBestSolutionsParameterName = "Best training solution";
        protected const string TrainingBestSolutionQualitiesParameterName = "Best training solution quality";

        private const string ValidationBestSolutionParameterName = "Best validation solution";
        private const string ValidationBestSolutionQualityParameterName = "Best validation solution quality";

        private const string LayerBestSolutionChartName = "Layer best solution chart";

        [StorableConstructor]
        protected LayerQualityAnalyzer(StorableConstructorFlag _) : base(_) { }
        protected LayerQualityAnalyzer(LayerQualityAnalyzer original, Cloner cloner)
          : base(original, cloner)
        {
        }

        protected LayerQualityAnalyzer() : base()
        {
        }

        public override IOperation Apply()
        {
            var results = ResultCollection;
            if (!results.ContainsKey(TrainingBestSolutionsParameterName)
                || !results.ContainsKey(ValidationBestSolutionParameterName))
                return base.Apply();

            #region Add Parameter
            if (!results.ContainsKey(LayerBestSolutionChartName))
                results.Add(new Result(LayerBestSolutionChartName, new DataTable(LayerBestSolutionChartName)));

            var layerQualityTable = (DataTable)results[LayerBestSolutionChartName].Value;

            if (!layerQualityTable.Rows.ContainsKey(TrainingBestSolutionQualitiesParameterName))
                layerQualityTable.Rows.Add(new DataRow(TrainingBestSolutionQualitiesParameterName));
            if (!layerQualityTable.Rows.ContainsKey(ValidationBestSolutionQualityParameterName))
                layerQualityTable.Rows.Add(new DataRow(ValidationBestSolutionQualityParameterName));
            #endregion

            layerQualityTable.Rows[TrainingBestSolutionQualitiesParameterName].Values.Add(((DoubleValue)results[TrainingBestSolutionQualitiesParameterName].Value).Value);
            layerQualityTable.Rows[ValidationBestSolutionQualityParameterName].Values.Add(((DoubleValue)results[ValidationBestSolutionQualityParameterName].Value).Value);
            
            return base.Apply();
        }
    }
}
