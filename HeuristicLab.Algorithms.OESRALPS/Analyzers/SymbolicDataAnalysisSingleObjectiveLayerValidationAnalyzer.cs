﻿using HEAL.Attic;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using HeuristicLab.Random;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [Item("SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer", "An operator that analyzes the validation best symbolic data analysis solution for single objective symbolic data analysis problems.")]
    [StorableType("DD82C026-CF68-40D7-A798-77EA61272AA9")]
    public abstract class SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer<T, U> : SymbolicDataAnalysisSingleObjectiveAnalyzer,
        ISymbolicDataAnalysisValidationAnalyzer<T, U>, IStochasticOperator
        where T : class, ISymbolicDataAnalysisSingleObjectiveEvaluator<U>
        where U : class, IDataAnalysisProblemData
    {
        private const string RandomParameterName = "GlobalRandom";
        private const string ProblemDataParameterName = "ProblemData";
        private const string EvaluatorParameterName = "Evaluator";
        private const string SymbolicDataAnalysisTreeInterpreterParameterName = "SymbolicExpressionTreeInterpreter";
        private const string ValidationPartitionParameterName = "ValidationPartition";
        private const string TestPartitionParameterName = "TestPartition";
        private const string RelativeNumberOfEvaluatedSamplesParameterName = "RelativeNumberOfEvaluatedSamples";
        private const string PercentageOfBestSolutionsParameterName = "PercentageOfBestSolutions";

        #region parameter properties
        public ILookupParameter<IRandom> RandomParameter {
            get { return (ILookupParameter<IRandom>)Parameters[RandomParameterName]; }
        }
        public ILookupParameter<U> ProblemDataParameter {
            get { return (ILookupParameter<U>)Parameters[ProblemDataParameterName]; }
        }
        public ILookupParameter<T> EvaluatorParameter {
            get { return (ILookupParameter<T>)Parameters[EvaluatorParameterName]; }
        }
        public ILookupParameter<ISymbolicDataAnalysisExpressionTreeInterpreter> SymbolicDataAnalysisTreeInterpreterParameter {
            get { return (ILookupParameter<ISymbolicDataAnalysisExpressionTreeInterpreter>)Parameters[SymbolicDataAnalysisTreeInterpreterParameterName]; }
        }
        public IValueLookupParameter<IntRange> ValidationPartitionParameter {
            get { return (IValueLookupParameter<IntRange>)Parameters[ValidationPartitionParameterName]; }
        }
        public IValueLookupParameter<IntRange> TestPartitionParameter {
            get { return (IValueLookupParameter<IntRange>)Parameters[TestPartitionParameterName]; }
        }
        public IValueLookupParameter<PercentValue> RelativeNumberOfEvaluatedSamplesParameter {
            get { return (IValueLookupParameter<PercentValue>)Parameters[RelativeNumberOfEvaluatedSamplesParameterName]; }
        }
        public IValueLookupParameter<PercentValue> PercentageOfBestSolutionsParameter {
            get { return (IValueLookupParameter<PercentValue>)Parameters[PercentageOfBestSolutionsParameterName]; }
        }
        #endregion

        [StorableConstructor]
        protected SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer(StorableConstructorFlag _) : base(_) { }
        protected SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer(SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer<T, U> original, Cloner cloner)
          : base(original, cloner)
        {
        }

        protected SymbolicDataAnalysisSingleObjectiveLayerValidationAnalyzer() : base()
        {
            Parameters.Add(new LookupParameter<IRandom>(RandomParameterName, "The random generator."));
            Parameters.Add(new LookupParameter<U>(ProblemDataParameterName, "The problem data of the symbolic data analysis problem."));
            Parameters.Add(new LookupParameter<T>(EvaluatorParameterName, "The operator to use for fitness evaluation on the validation partition."));
            Parameters.Add(new LookupParameter<ISymbolicDataAnalysisExpressionTreeInterpreter>(SymbolicDataAnalysisTreeInterpreterParameterName, "The interpreter for symbolic data analysis expression trees."));
            Parameters.Add(new ValueLookupParameter<IntRange>(ValidationPartitionParameterName, "The validation partition."));
            Parameters.Add(new ValueLookupParameter<IntRange>(TestPartitionParameterName, "The test partition."));
            Parameters.Add(new ValueLookupParameter<PercentValue>(RelativeNumberOfEvaluatedSamplesParameterName, "The relative number of samples of the dataset partition, which should be randomly chosen for evaluation between the start and end index."));
            Parameters.Add(new ValueLookupParameter<PercentValue>(PercentageOfBestSolutionsParameterName, "The percentage of the top solutions which should be analyzed.", new PercentValue(0.25)));
        }

        protected virtual IEnumerable<int> GenerateRowsToEvaluate()
        {
            if (ValidationPartitionParameter.ActualValue == null)
                return Enumerable.Empty<int>();

            int seed = RandomParameter.ActualValue.Next();
            int samplesStart = ValidationPartitionParameter.ActualValue.Start;
            int samplesEnd = ValidationPartitionParameter.ActualValue.End;
            int testPartitionStart = ProblemDataParameter.ActualValue.TestPartition.Start;
            int testPartitionEnd = ProblemDataParameter.ActualValue.TestPartition.End;

            if (samplesEnd < samplesStart) throw new ArgumentException("Start value is larger than end value.");
            int count = (int)((samplesEnd - samplesStart) * RelativeNumberOfEvaluatedSamplesParameter.ActualValue.Value);
            if (count == 0) count = 1;
            return RandomEnumerable.SampleRandomNumbers(seed, samplesStart, samplesEnd, count)
              .Where(i => i < testPartitionStart && i < ProblemDataParameter.ActualValue.Dataset.Rows);
        }
    }
}
