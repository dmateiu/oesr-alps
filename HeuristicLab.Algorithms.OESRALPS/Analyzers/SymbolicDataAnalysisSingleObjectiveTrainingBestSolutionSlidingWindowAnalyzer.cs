﻿using HEAL.Attic;
using HeuristicLab.Collections;
using HeuristicLab.Common;
using HeuristicLab.Core;
using HeuristicLab.Data;
using HeuristicLab.Encodings.SymbolicExpressionTreeEncoding;
using HeuristicLab.Operators;
using HeuristicLab.Optimization;
using HeuristicLab.Parameters;
using HeuristicLab.Problems.DataAnalysis;
using HeuristicLab.Problems.DataAnalysis.Symbolic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HeuristicLab.Algorithms.OESRALPS.Analyzers
{
    [Item("SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer", "An operator that analyzes the training best symbolic data analysis solution for single objective symbolic data analysis problems.")]
    [StorableType("DD82C026-CF68-40D7-A898-77EA61272DE9")]
    public abstract class SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer<S, T, U> 
        : SymbolicDataAnalysisSingleObjectiveAnalyzer, IIterationBasedOperator
        where S : class, ISymbolicDataAnalysisSolution
        where T : class, ISymbolicDataAnalysisSingleObjectiveEvaluator<U>
        where U : class, IDataAnalysisProblemData
    {
        private const string TrainingBestSolutionParameterName = "Best training solution";
        private const string TrainingBestSolutionQualityParameterName = "Best training solution quality";
        private const string TrainingBestSolutionGenerationParameterName = "Best training solution generation";
        private const string TrainingBestSolutionsHistoryParameterName = "Best training solutions history";
        private const string UpdateAlwaysParameterName = "Always update best solution";
        private const string IterationsParameterName = "Generations";
        private const string MaximumIterationsParameterName = "Maximum Iterations";
        private const string StoreHistoryParameterName = "Store History";

        private const string TrainingPartitionParameterName = "TrainingPartition";

        private const string RandomParameterName = "GlobalRandom";
        private const string ProblemDataParameterName = "ProblemData";
        private const string EvaluatorParameterName = "Evaluator";
        private const string SymbolicDataAnalysisTreeInterpreterParameterName = "SymbolicExpressionTreeInterpreter";

        private bool isTrainingEventInitialized = false;
        private bool reevaluateTrainingResult = false;

        #region parameter properties
        public ILookupParameter<S> TrainingBestSolutionParameter {
            get { return (ILookupParameter<S>)Parameters[TrainingBestSolutionParameterName]; }
        }
        public ILookupParameter<DoubleValue> TrainingBestSolutionQualityParameter {
            get { return (ILookupParameter<DoubleValue>)Parameters[TrainingBestSolutionQualityParameterName]; }
        }
        public ILookupParameter<IntValue> TrainingBestSolutionGenerationParameter {
            get { return (ILookupParameter<IntValue>)Parameters[TrainingBestSolutionGenerationParameterName]; }
        }
        public ILookupParameter<ItemList<S>> TrainingBestSolutionsHistoryParameter {
            get { return (ILookupParameter<ItemList<S>>)Parameters[TrainingBestSolutionsHistoryParameterName]; }
        }
        public IFixedValueParameter<BoolValue> UpdateAlwaysParameter {
            get { return (IFixedValueParameter<BoolValue>)Parameters[UpdateAlwaysParameterName]; }
        }
        public ILookupParameter<IntValue> IterationsParameter {
            get { return (ILookupParameter<IntValue>)Parameters[IterationsParameterName]; }
        }
        public IValueLookupParameter<IntValue> MaximumIterationsParameter {
            get { return (IValueLookupParameter<IntValue>)Parameters[MaximumIterationsParameterName]; }
        }

        public IFixedValueParameter<BoolValue> StoreHistoryParameter {
            get { return (IFixedValueParameter<BoolValue>)Parameters[StoreHistoryParameterName]; }
        }
        public IValueLookupParameter<IntRange> TrainingPartitionParameter {
            get { return (IValueLookupParameter<IntRange>)Parameters[TrainingPartitionParameterName]; }
        }
        public ILookupParameter<IRandom> RandomParameter {
            get { return (ILookupParameter<IRandom>)Parameters[RandomParameterName]; }
        }
        public ILookupParameter<U> ProblemDataParameter {
            get { return (ILookupParameter<U>)Parameters[ProblemDataParameterName]; }
        }
        public ILookupParameter<T> EvaluatorParameter {
            get { return (ILookupParameter<T>)Parameters[EvaluatorParameterName]; }
        }
        public ILookupParameter<ISymbolicDataAnalysisExpressionTreeInterpreter> SymbolicDataAnalysisTreeInterpreterParameter {
            get { return (ILookupParameter<ISymbolicDataAnalysisExpressionTreeInterpreter>)Parameters[SymbolicDataAnalysisTreeInterpreterParameterName]; }
        }
        #endregion

        #region properties
        public S TrainingBestSolution {
            get { return TrainingBestSolutionParameter.ActualValue; }
            set { TrainingBestSolutionParameter.ActualValue = value; }
        }
        public DoubleValue TrainingBestSolutionQuality {
            get { return TrainingBestSolutionQualityParameter.ActualValue; }
            set { TrainingBestSolutionQualityParameter.ActualValue = value; }
        }
        public bool UpdateAlways {
            get { return UpdateAlwaysParameter.Value.Value; }
            set { UpdateAlwaysParameter.Value.Value = value; }
        }

        public bool StoreHistory {
            get { return StoreHistoryParameter.Value.Value; }
            set { StoreHistoryParameter.Value.Value = value; }
        }
        #endregion

        [StorableConstructor]
        protected SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer(StorableConstructorFlag _) : base(_) { }
        protected SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer(SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer<S, T, U> original, Cloner cloner) : base(original, cloner) { }
        public SymbolicDataAnalysisSingleObjectiveTrainingBestSolutionSlidingWindowAnalyzer()
          : base()
        {
            Parameters.Add(new LookupParameter<IRandom>(RandomParameterName, "The random generator."));
            Parameters.Add(new LookupParameter<U>(ProblemDataParameterName, "The problem data of the symbolic data analysis problem."));
            Parameters.Add(new LookupParameter<T>(EvaluatorParameterName, "The operator to use for fitness evaluation on the validation partition."));
            Parameters.Add(new LookupParameter<ISymbolicDataAnalysisExpressionTreeInterpreter>(SymbolicDataAnalysisTreeInterpreterParameterName, "The interpreter for symbolic data analysis expression trees."));

            Parameters.Add(new LookupParameter<S>(TrainingBestSolutionParameterName, "The best training symbolic data analyis solution."));
            Parameters.Add(new LookupParameter<DoubleValue>(TrainingBestSolutionQualityParameterName, "The quality of the training best symbolic data analysis solution."));
            Parameters.Add(new LookupParameter<IntValue>(TrainingBestSolutionGenerationParameterName, "The generation in which the best training solution was found."));
            Parameters.Add(new FixedValueParameter<BoolValue>(UpdateAlwaysParameterName, "Determines if the best training solution should always be updated regardless of its quality.", new BoolValue(true)));
            Parameters.Add(new LookupParameter<IntValue>(IterationsParameterName, "The number of performed iterations."));
            Parameters.Add(new ValueLookupParameter<IntValue>(MaximumIterationsParameterName, "The maximum number of performed iterations.") { Hidden = true });
            Parameters.Add(new FixedValueParameter<BoolValue>(StoreHistoryParameterName, "Flag that determines whether all encountered best solutions should be stored as results.", new BoolValue(false)));
            Parameters.Add(new LookupParameter<ItemList<S>>(TrainingBestSolutionsHistoryParameterName, "The history of the best training symbolic data analysis solutions."));

            Parameters.Add(new ValueLookupParameter<IntRange>(TrainingPartitionParameterName, "The current training sliding window position or range."));
            UpdateAlwaysParameter.Hidden = true;
        }

        public override IOperation Apply()
        {
            var results = ResultCollection;
            #region create results
            if (!results.ContainsKey(TrainingBestSolutionParameter.Name))
                results.Add(new Result(TrainingBestSolutionParameter.Name, TrainingBestSolutionParameter.Description, typeof(S)));
            if (!results.ContainsKey(TrainingBestSolutionQualityParameter.Name))
                results.Add(new Result(TrainingBestSolutionQualityParameter.Name, TrainingBestSolutionQualityParameter.Description, typeof(DoubleValue)));
            if (!results.ContainsKey(TrainingBestSolutionGenerationParameter.Name) && IterationsParameter.ActualValue != null)
                results.Add(new Result(TrainingBestSolutionGenerationParameter.Name, TrainingBestSolutionGenerationParameter.Description, typeof(IntValue)));
            if (StoreHistory && !results.ContainsKey(TrainingBestSolutionsHistoryParameter.Name))
            {
                results.Add(new Result(TrainingBestSolutionsHistoryParameter.Name, TrainingBestSolutionsHistoryParameter.Description, typeof(ItemList<S>)));
                TrainingBestSolutionsHistoryParameter.ActualValue = new ItemList<S>();
                results[TrainingBestSolutionsHistoryParameter.Name].Value = TrainingBestSolutionsHistoryParameter.ActualValue;
            }
            #endregion

            if (TrainingPartitionParameter.ActualValue != null && !isTrainingEventInitialized) {
                TrainingPartitionParameter.ActualValue.ValueChanged += TrainingPartition_ValueChanged;
                isTrainingEventInitialized = true;
            }

            #region find best tree
            double bestQuality = Maximization.Value ? double.NegativeInfinity : double.PositiveInfinity;
            ISymbolicExpressionTree bestTree = null;
            ISymbolicExpressionTree[] tree = SymbolicExpressionTree.ToArray();
            double[] quality = Quality.Select(x => x.Value).ToArray();
            for (int i = 0; i < tree.Length; i++)
            {
                if (IsBetter(quality[i], bestQuality, Maximization.Value))
                {
                    bestQuality = quality[i];
                    bestTree = tree[i];
                }
            }
            #endregion

            if (TrainingPartitionParameter.ActualValue != null && (UpdateAlways || (reevaluateTrainingResult
                && TrainingBestSolutionQuality != null 
                && TrainingPartitionParameter.ActualValue != null)))
            {
                var evaluator = EvaluatorParameter.ActualValue;
                var problemData = ProblemDataParameter.ActualValue;

                IExecutionContext childContext = (IExecutionContext)ExecutionContext.CreateChildOperation(evaluator);
                var currentValidationBestSolution = (S)results[TrainingBestSolutionParameterName].Value;
                TrainingBestSolutionQuality.Value =
                    evaluator.Evaluate(
                        childContext, 
                        currentValidationBestSolution.Model.SymbolicExpressionTree, 
                        problemData,
                        Enumerable.Range(
                            TrainingPartitionParameter.ActualValue.Start, 
                            TrainingPartitionParameter.ActualValue.End - TrainingPartitionParameter.ActualValue.Start
                        ));
                results[TrainingBestSolutionQualityParameter.Name].Value = TrainingBestSolutionQuality;
                results[TrainingBestSolutionGenerationParameter.Name].Value = IterationsParameter.ActualValue;
                reevaluateTrainingResult = false;
            }

            if (bestTree != null && (UpdateAlways || TrainingBestSolutionQuality == null ||
              IsBetter(bestQuality, TrainingBestSolutionQuality.Value, Maximization.Value)))
            {
                TrainingBestSolution = CreateSolution(bestTree, bestQuality);
                TrainingBestSolutionQuality = new DoubleValue(bestQuality);

                if (IterationsParameter.ActualValue != null)
                    TrainingBestSolutionGenerationParameter.ActualValue = new IntValue(IterationsParameter.ActualValue.Value);

                results[TrainingBestSolutionParameter.Name].Value = TrainingBestSolution;
                results[TrainingBestSolutionQualityParameter.Name].Value = TrainingBestSolutionQuality;
                if (TrainingBestSolutionGenerationParameter.ActualValue != null)
                    results[TrainingBestSolutionGenerationParameter.Name].Value = TrainingBestSolutionGenerationParameter.ActualValue;

                if (StoreHistory)
                {
                    TrainingBestSolutionsHistoryParameter.ActualValue.Add(TrainingBestSolution);
                }
            }

            UniformSubScopesProcessor subScopesProcessor = new UniformSubScopesProcessor();
            subScopesProcessor.Operator = EvaluatorParameter.ActualValue;
            subScopesProcessor.Depth.Value = 1;
            var operation = ExecutionContext.CreateChildOperation(subScopesProcessor);
            var successor = base.Apply();

            return new OperationCollection() { operation, successor };
        }

        private void TrainingPartition_ValueChanged(object sender, EventArgs e)
        {
            reevaluateTrainingResult = true;
        }

        protected abstract S CreateSolution(ISymbolicExpressionTree bestTree, double bestQuality);

        private bool IsBetter(double lhs, double rhs, bool maximization)
        {
            if (maximization) return lhs > rhs;
            else return lhs < rhs;
        }
    }
}
